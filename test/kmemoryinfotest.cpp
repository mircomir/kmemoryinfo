/*
    This file is part of the KDE Frameworks

    SPDX-FileCopyrightText: 2022 Mirco Miranda

    SPDX-License-Identifier: BSD-3-Clause
*/

#include "kmemoryinfotest.h"

#include <QtTest/QtTest>

#include "../src/kmemoryinfo.h"

KMemoryInfoTest::KMemoryInfoTest(QObject *parent) : QObject(parent)
{

}

void KMemoryInfoTest::isNull()
{
    KMemoryInfo m;
    QCOMPARE(m.isNull(), false);
}

void KMemoryInfoTest::operators()
{
    KMemoryInfo m;
    auto m1 = m;
    QVERIFY(m == m1);

    // paranoia check
    QVERIFY(m.totalPhysical() != 0);
    QCOMPARE(m.totalPhysical(), m1.totalPhysical());
}
