/*
 * File         : %{Cpp:License:FileName}
 * Author       : mircomir
 * Created on   : 2022/5/12
 * Modified on  :
 * Copyright    : System Ceramics
 * Site         : www.systemceramics.com
 * Class        : %{Cpp:License:ClassName}
 *
 * Description  : [ write here the class/module description ]
 */
#include <QCoreApplication>
#include <QDebug>
#include <QTimer>
#include <QtTest/QtTest>

#include "src/kmemoryinfo.h"
#include "test/kmemoryinfotest.h"

#define KIB(bytes)      (qint64(bytes) / 1024ll)
#define MIB(bytes)      (   KIB(bytes) / 1024ll)
#define GIB(bytes)      (   MIB(bytes) / 1024ll)

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    KMemoryInfo m;
    qDebug() << "Error while collecting data:" << m.isNull();
    qDebug() << "";
    qDebug() << "Total RAM:" << MIB(m.totalPhysical()) << "MiB";
    qDebug() << "Avail RAM:" << MIB(m.availablePhysical()) << "MiB";
    qDebug() << "Free  RAM:" << MIB(m.freePhysical()) << "MiB";
    qDebug() << "Buffers:" << MIB(m.buffers()) << "MiB";
    qDebug() << "Cached:" << MIB(m.cached()) << "MiB";
    qDebug() << "";
    qDebug() << "Total Page File:" << MIB(m.totalSwapFile()) << "MiB";
    qDebug() << "Free  Page File:" << MIB(m.freeSwapFile()) << "MiB";
    qDebug() << "";

    // TODO: write a QTest class
    QTest::qExec(new KMemoryInfoTest());

    QTimer::singleShot(100, &a, SLOT(quit()));
    return a.exec();
}
