/*
 * File         : kcoreaddons_export.h
 * Author       : mircomir
 * Created on   : 2022/5/14
 * Description  : Fake file to compile in a separate project.
 */
#ifndef KCOREADDONS_EXPORT_H
#define KCOREADDONS_EXPORT_H

#define KCOREADDONS_EXPORT

#endif // KCOREADDONS_EXPORT_H
